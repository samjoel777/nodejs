var fs = require('fs');

var path = 'H:/NodeJs/file_operations/data_directory/data.txt'

var data = "Hello World"

// fs write (Asynchronous)

console.log("Start");

fs.writeFile(path,data,(error)=>{
    if(error) console.log(error);
    console.log("Writing File");
});

console.log("End");
