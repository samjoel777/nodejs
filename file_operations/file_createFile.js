var fs = require('fs');

var path = 'H:/NodeJs/file_operations/data_directory/data_new3.txt'


// fs createFile --> 1.appendFile() 2.open() 3. writeFile()

console.log("Start");

// 1
// fs.appendFile(path,"Hello",(error)=>{
//     if(error) throw error;
//     console.log("Done");
// });

// 2
// fs.open(path,"w",(error)=>{
//     if(error) throw error;
//     console.log("Done");
// });

// 3
fs.writeFile(path,"Hello",(error)=>{
    if(error) throw error;
    console.log("Done");
});

console.log("End");