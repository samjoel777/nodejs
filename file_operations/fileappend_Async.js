var fs = require('fs');

var path = 'H:/NodeJs/file_operations/data_directory/data.txt'

var data = "Hello World, I'am SAM JOEL";

// fs append (Asynchronous)

console.log("Start");

fs.appendFile(path,"\n"+data,(error)=>{
    if(error) console.log(error);
    console.log("Done Writing");
});

console.log("End")
