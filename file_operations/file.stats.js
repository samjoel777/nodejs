var fs = require('fs');

var path = 'H:/NodeJs/file_operations/data_directory/data.txt'


// Synchronous

// console.log("Start");

// var res = fs.statSync(path);

// console.log(res);

// console.log("End");

// Synchronous

console.log("Start");

fs.stat(path,(error,stats)=>{
    if(error) throw error;
    console.log(stats); 
});

console.log("End");

//  fstat() is identical to stat(), except that the file to be stat-ed is specified by the file descriptor fd.

// lstat() is identical to stat(), except that if path is a symbolic link, then the link itself is stat-ed, not the file that it refers to.