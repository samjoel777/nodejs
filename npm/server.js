// require('H:/NodeJs/npm/app/models/connection').open();

require('H:/NodeJs/npm/app/models/conn.js')

const express = require('express');

var bodyParser = require('body-parser');

var app = express();

var CONFIG = require('H:/NodeJs/npm/app/config/index.js')

var homeRoutes = require('H:/NodeJs/npm/app/routes/home.route.js');

var productRoutes = require('H:/NodeJs/npm/app/routes/product.route.js');

var userRoutes = require('H:/NodeJs/npm/app/routes/user.route.js');

app.use('/',homeRoutes);

app.use('/api',productRoutes);

app.use('/api',userRoutes)

// parse application/json
// app.use(bodyParser.json())

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json({ type: 'application/*+json' }));

const port = 3080;

const hostname = '127.0.0.1';

app.listen(CONFIG.PORT,CONFIG.HOST,()=>{
    console.log(`Server is running on port:${CONFIG.PORT}`);
});

