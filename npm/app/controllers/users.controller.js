var mongoose = require('mongoose');

var User = mongoose.model('User')

module.exports.addOneUser = (req,res,next) =>{
    console.log(req.body);
    User.create(req.body,(error,resp)=>{
        if(error){
            res
            .status(500)
            .json({
                msg : "Failed to insert Data",
                error : error
            })
        }else{
            res
            .status(200)
            .json({
                message : "Inserted Data Successfully",
                resp : resp
            })
        }
    })
}