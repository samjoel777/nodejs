module.exports.getRoot = (req,res)=>{
    console.log("---------------");
    console.log(req.url);
    console.log("---------------");
    console.log(req.params)

    res
    .status(200)
    .send({
        message : "This is a Root Directory",
        qs : req.params
    })
};

module.exports.getHome = (req,res)=>
{
    console.log("---------------");
    console.log(req.url);
    console.log("---------------");
    console.log(req.params)

    res
    .status(200)
    .send({
        message : "This is a Home Directory",
        qs : req.params
    })
};