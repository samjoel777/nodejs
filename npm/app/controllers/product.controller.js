var conn = require('H:/NodeJs/npm/app/models/connection.js')

var data = require('H:/NodeJs/npm/app/models/data/productdetails.json')

var ObjectId = require('mongodb').ObjectId;

module.exports.getProducts =  (req,res)=>{
    console.log(req.query);

    var offset = (req.query.offset)?parseInt(req.query.offset,10):0
    var count  = (req.query.count)?parseInt(req.query.count,10):5

    collection = conn.get().db('mcd_db').collection('test_2')
    collection.find({}).skip(offset).limit(count).toArray((error,docs)=>{
        if(error){
            res
            .status(500)
            .json({
                msg: "Failed to retrieve data",
                error : error
            });
        }else{
            console.log("doc length:",docs.length)
            res
            .status(200)
            .json(docs)
        }
    });
};

module.exports.getOneProduct = (req,res)=>{
    var productId = req.params.productId;
    console.log("productId:",productId);

    collection = conn.get().db('mcd_db').collection('test_2')
    collection.findOne({"_id": ObjectId(productId)},(error,doc)=>{
        if(error){
            res
            .status(500)
            .json({
                message : "Failed to retrieve data from id"+productId,
                error : error
            })
        }else{
            res
            .status(200)
            .json(doc)
        }
    })
}

module.exports.addProducts = (req,res)=>{
    collection = conn.get().db('mcd_db').collection('test_2')
    collection.insertMany(data,(error,resp)=>{
        if(error){
            res
            .status(500)
            .json({
                msg: "Failed to insert data",
                error : error
            });
        }else{
            res
            .status(200)
            .json({
                message: "Records are Inserted Successfully",
                qs : req.query 
            })
        }
    });
};

module.exports.addOneProduct = (req,res)=>{
    collection = conn.get().db('mcd_db').collection('test_2')
    collection.insertOne(req.body,(error,resp)=>{
        if(error){
            res
            .status(500)
            .json({
                msg: "Failed to insert data",
                error : error
        })
        }else{
            res
            .status(200)
            .json({
                message: "Records are Inserted Successfully",
                res : resp
            })
        }
    })
}; 

module.exports.addAnotherProduct = (req,res)=>{
    console.log('------------------------');
    console.log(req.url);
    console.log('------------------------');
    console.log(req.body);
    console.log(req.headers);
    

    res
    .status(200)
    .json({
        message: "This is a POST METHOD",
        qs : res.body,
        qs : res.headers
    })
};

module.exports.updateOneProduct = (req,res)=>{
    console.log(req.params);
    console.log(req.body);
    var productId = req.params.productId;
    if(productId){
        collection = conn.get().db('mcd_db').collection('test_2')
        var filterQ = {"_id:":ObjectId(productId)}
        var updateQ = {
            $set:{
                "name": req.body.name,
                "image": req.body.image
            }
        }
        collection.update(filterQ,updateQ,(error,resp)=>{
            if(error){
                res
                .status(500)
                .json({
                    msg: "Failed to Update data",
                    error : error
            })
            }else{
                res
                .status(200)
                .json({
                    message: "Records are Inserted Successfully",
                    res : resp
                })
            }
        })
    }else{
        res
        .status(404)
        .json({
            msg:"Product Id is not found",
            productId : req.params.productId
        })
    }
};