var router = require('express').Router();

var homeCtrl = require('H:/NodeJs/npm/app/controllers/home.controller.js')

router
.route('/')
.get(homeCtrl.getRoot)

router
.route('/home')
.get(homeCtrl.getHome)

module.exports = router;