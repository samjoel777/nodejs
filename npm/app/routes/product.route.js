var router = require('express').Router();

var productCtrl = require('H:/NodeJs/npm/app/controllers/product.controller.js')

router
.route('/product')
.get(productCtrl.getProducts)

router
.route('/product')
.post(productCtrl.addProducts)

router
.route('/product/new')
.post(productCtrl.addOneProduct)

router
.route('/product/old')
.post(productCtrl.addAnotherProduct)

router
.route('/product/:productId')
.get(productCtrl.getOneProduct)
.put(productCtrl.updateOneProduct)

module.exports = router;