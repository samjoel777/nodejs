var mongoClient = require('mongodb').MongoClient;

var dburl = 'mongodb://app_root:app121@localhost:27017/admin'

var rdburl = "mongodb://app_test:app121@test-shard-00-00-k7qze.mongodb.net:27017,test-shard-00-01-k7qze.mongodb.net:27017,test-shard-00-02-k7qze.mongodb.net:27017/test?ssl=true&replicaSet=test-shard-0&authSource=admin&retryWrites=true"

var connection;

var open = () =>{
    mongoClient.connect(dburl,{ useNewUrlParser: true },(error,client)=>{
        if(error){
            console.log("MongoDb Connection Failed");
        }
        else{
            console.log("MongoDb Connection Success");
            connection = client;
        }
    });
}

var get = () =>{
    return connection;
}

module.exports = {
    open : open,
    get : get
}