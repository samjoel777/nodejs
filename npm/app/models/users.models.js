var mongoose = require('mongoose');

var Schema  = mongoose.Schema;

var userSchema = new Schema({
    name : String,
    email : String,
    password : String,
    phone : Number,
    age : Number,
    address : {
        street : String,
        pincode : Number,
        state : String,
        city : String,
        country : String,
    },
    gender : String

})

mongoose.model('User',userSchema,'tw_accounts')