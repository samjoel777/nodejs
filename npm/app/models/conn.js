var CONFIG = require('H:/NodeJs/npm/app/config/index.js')

const mongoose = require('mongoose');

require('H:/NodeJs/npm/app/models/users.models.js')

var dbconfig = {
    user : CONFIG.DBUSER,
    pass : CONFIG.DBPASS,
    authSource : 'admin',
    useNewUrlParser : true
}

mongoose.connect(CONFIG.DBURL,dbconfig);

var conn = mongoose.connection;

conn.on('error',(error)=>{
    console.log("DB Connection Error");
    console.log(error);
});

conn.once('open',()=>{
    console.log("DB Connection Success");
    
});