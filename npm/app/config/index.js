const host = '127.0.0.1'

const port = 3080

const dburl = 'mongodb://localhost:27017/admin'

const dbuser = 'app_root'

const dbpass = 'app121'

module.exports = {
    HOST : host,
    PORT : port,
    DBURL : dburl,
    DBUSER : dbuser,
    DBPASS : dbpass
}