var find = require('./find.js');

var takeOrder = (name) =>
{
    var res = find.findOrder(name);
    if(res)
    {
        console.log("Order is place for");
        console.log(`
        ${res.name}
        ${res.size}
        ${res.cost}
        `);
    }
    else{
        console.log("pizza is not there in the menu");
    }
}

module.exports = { takeOrder: takeOrder }
