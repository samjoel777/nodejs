var pizzas = require('./pizzas')

module.exports.findOrder = (name) => 
{
    var isFound = false;
    for(pizza of pizzas)
    {
        if(pizza.name === name)
        {
            return pizza;
        }
        else{
            isFound = true;
        }
    }
    if(isFound){
        return 0 ;
    }
}

