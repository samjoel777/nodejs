var http = require('http');

const port = 3070;

const host = "127.0.0.1";

var server = http.createServer((request,response)=>{
    console.log("url:",request.url);

    if(request.url === '/')
    {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
        response.end('Hello World\n');
    }

    else if(request.url === '/user')
    {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
        response.end('User Data\n');
    }

    else if(request.url === '/data')
    {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
        response.end('General Data\n');
    }

    else if(request.url === '/json')
    {
        var user ={
            name : "SAM",
            age : 25,
            gender : "Male"
        }
        response.statusCode = 200;
        response.setHeader('Content-Type', 'application/plain');
        response.end(JSON.stringify(user));
    }

    else if(request.url === '/file')
    {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
        response.end('File Data\n');
    }

    else
    {
        response.statusCode = 404;
        response.setHeader('Content-Type', 'text/plain');
        response.end('Not Found');
    }

});

server.listen(port,host,()=>{
    console.log(`Server running at http://${host}:${port}/`);
});